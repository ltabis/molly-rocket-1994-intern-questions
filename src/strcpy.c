#include <stdlib.h>
#include <stdio.h>

char *my_strcpy(const char *from, char *to)
{
  // NOTE: no need to check if the strings overlap ?
  // NOTE: no need to check if the pointers are allocated ?
  unsigned int i = 0;

  for (; from[i] != '\0'; ++i) {
    to[i] = from[i];
  }

  // Copy null terminator.
  to[i] = from[i];

  return to;
}

int main() {

  char *from = "Hello, World!\0";
  char *to = malloc(sizeof(char) * 14);
  char *dest = my_strcpy(from, to);

  printf("%s", dest);

  free(to);

  return 0;
}
