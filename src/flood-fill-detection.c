#include <stdio.h>
#include <assert.h>

// NOTE: this solution is wrong: the color parameter should
//       be a two bit number, not a full integer.
//       Also, since the color is stored on only two bits,
//       it could return true when two two bits sections would
//       cross. (i.e. 0b11 for 0b0110 would be true even though
//       it is wrong)
int fast_color_check(int color, int data) {
  return (data & color) != 0;
}

int main() {

  // Those examples are wrong.
  int r1 = fast_color_check(0b0011, 0b1111);
  int r2 = fast_color_check(0b0011, 0b1100);
  int r3 = fast_color_check(0b11100011, 0b00100000);
  int r4 = fast_color_check(0b11100011, 0b11000000);
  int r5 = fast_color_check(0b11100011, 0b00000000);

  assert(r1 == 1);
  assert(r2 == 0);
  assert(r3 == 1);
  assert(r4 == 1);
  assert(r5 == 0);
  
  return 0;
}
