#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RECT_SIZE 30
#define RECT_PITCH 10

/// Copy a rectangle of memory from the buffer `from` to the buffer `to`.
/// NOTE: x_max > x_min, y_max > y_min.
/// NOTE: from and to must be initialized.
void rectangle_copy(char *from, int from_pitch,
	  char *to, int to_pitch,
	  int x_min, int y_min, int x_max, int y_max,
	  int to_x, int to_y)
{
  int rect_width = x_max - x_min + 1;
  int to_offset = to_y * rect_width + to_x;

  printf("rect_width: %i, to_offset: %i\n", rect_width, to_offset);

  for (int y = y_min; y <= y_max; ++y) {
    int offset = sizeof(char) * (y * from_pitch + x_min);
    printf("offset: %i\n", offset);
    memcpy(to + to_offset + offset, from + offset, rect_width);
  }
}

void debug_rectangle(char *rect, int rect_pitch, int until) {
  for (int y = 0;; ++y) {
    for (int x = 0; x < rect_pitch; ++x) {
      if (y * rect_pitch + x >= until) {
	printf("\n");
	return;
      }
      printf("%i", rect[y * rect_pitch + x]);
    }
    printf("\n");
  }
}

int main() {
  char *from = malloc(sizeof(char) * RECT_SIZE);
  char *to = malloc(sizeof(char) * RECT_SIZE);

  memset(from, 0, sizeof(char) * RECT_SIZE);
  memset(to, 0, sizeof(char) * RECT_SIZE);

  from[0 * RECT_PITCH + 5] = 1;
  from[0 * RECT_PITCH + 6] = 1;
  from[0 * RECT_PITCH + 7] = 1;
  from[1 * RECT_PITCH + 5] = 1;
  from[1 * RECT_PITCH + 6] = 1;
  from[1 * RECT_PITCH + 7] = 1;
  from[2 * RECT_PITCH + 5] = 1;
  from[2 * RECT_PITCH + 6] = 1;
  from[2 * RECT_PITCH + 7] = 1;

  printf("from:\n");
  debug_rectangle(from, RECT_PITCH, RECT_SIZE);
  printf("to:\n");
  debug_rectangle(to, RECT_PITCH, RECT_SIZE);

  rectangle_copy(from, RECT_PITCH, to, RECT_PITCH, 5, 0, 7, 1, 0, 0);

  printf("copied to:\n");
  debug_rectangle(to, RECT_PITCH, RECT_SIZE);

  free(from);
  free(to);

  return 0;
}
