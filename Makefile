SRC_RCPY = src/rectangle-copy.c
OBJ_RCPY = $(SRC_RCPY:.c=.o)
SRC_STRCPY = src/strcpy.c
OBJ_STRCPY = $(SRC_STRCPY:.c=.o)
SRC_FFD = src/flood-fill-detection.c
OBJ_FFD = $(SRC_FFD:.c=.o)
CFLAGS = -Wall -Wextra

all: rectangle-copy strcpy

rectangle-copy: $(OBJ_RCPY)
	cc -o rectangle-copy $(OBJ_RCPY) $(CFLAGS)

strcpy: $(OBJ_STRCPY)
	cc -o strcpy $(OBJ_STRCPY) $(CFLAGS)

flood-fill-detection: $(OBJ_FFD)
	cc -o flood-fill-detection $(OBJ_FFD) $(CFLAGS)

clean:
	rm src/*.o

fclean: clean
	rm rectangle-copy strcpy

re: fclean all
